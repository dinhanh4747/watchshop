import db from "../models/index"

let getHomePage = async (req, res) => {
    try {
        let data = await db.User.findAll();
        console.log('-------------------');
        console.log(data);
        return res.render('homePage.ejs', { 
            data: JSON.stringify(data) 
        });
    } catch (error) {
        console.log(error);
    }

}

let getAboutMe = (req, res) => {
    return res.render('about/about.ejs');
}

module.exports = {
    getHomePage,
    getAboutMe,
}